from behave import *
import os
from datetime import datetime
import uuid
from features.utils.deck_utils import *
from features.utils.db_utils import *



@step
@Given('I open deck by URL "{deck_name}"')
def step_impl(context, deck_name):
    context.deck_content_page.open_deck_page(deck_name)


@step
@Given('I download deck from decks_list.json by name "{deck_name}"')
def step_impl(context, deck_name):
    context.deck_content_page.open_deck_page(deck_name)
    save_deck_as_json(context, deck_name)


@step
@Given('I download deck "{deck_name}" and additional cards from other decks in list and merge these to print')
def step_impl(context, deck_name):
    for deck in get_all_decks_urls_from_deck_list_file():
        context.deck_content_page.open_deck_page(deck)
        save_deck_as_json(context, deck)
    for deck in context.list_of_decks:
        if not deck == deck_name:
            for card in context.list_of_decks[deck].cards:
                if not context.list_of_decks[deck_name].is_card_in_deck(card):
                    context.list_of_decks[deck_name].add_card(card)
    objective_cards_list, gambit_cards_list = list(), list()
    deck_obj = context.list_of_decks[deck_name]
    output_folder = context.file_utils.for_printing_folder
    context.file_utils.create_all_dirs_along_filepath(output_folder)
    context.file_utils.merge_all_cards_to_print(context, deck_obj, objective_cards_list, gambit_cards_list)
    obj_name, gambit_name = deck_name + '_object_', deck_name + '_gambit_'
    context.file_utils.image_combiner(obj_name, objective_cards_list, (532, 744), 2)
    context.file_utils.image_combiner(gambit_name, gambit_cards_list, (532, 744), 2)


@step
@Given('I save deck into DB from decks_list.json by name "{deck_name}"')
def step_impl(context, deck_name):
    context.deck_content_page.open_deck_page(deck_name)
    save_dict_to_db(get_dict_from_deck_obj(context, deck_name))


@step
@Given('I download all decks from decks_list.json')
def step_impl(context):
    all_decks_in_config = get_all_decks_urls_from_deck_list_file()
    for deck in all_decks_in_config:
        context.deck_content_page.open_deck_page(deck)
        save_deck_as_json(context, deck)


@step
@Then('I download all cards of all decks')
def step_impl(context):
    context.file_utils.save_all_cards_images_of_all_decks(context)


@step
@When('I merge to print all cards from deck "{deck_name}"')
def step_impl(context, deck_name):
    objective_cards_list, gambit_cards_list = list(), list()
    deck_obj = load_deck(context, deck_name)
    output_folder = context.file_utils.for_printing_folder
    context.file_utils.create_all_dirs_along_filepath(output_folder)
    context.file_utils.merge_all_cards_to_print(context, deck_obj, objective_cards_list, gambit_cards_list)
    obj_name, gambit_name = deck_name + '_object_', deck_name + '_gambit_'
    context.file_utils.image_combiner(obj_name, objective_cards_list, (532, 744), 2)
    context.file_utils.image_combiner(gambit_name, gambit_cards_list, (532, 744), 2)


@step
@When('I merge to print all cards from all decks in config file')
def step_impl(context):
    objective_cards_list, gambit_cards_list = list(), list()
    all_decks_in_config = get_all_decks_urls_from_deck_list_file()
    output_folder = context.file_utils.for_printing_folder
    context.file_utils.create_all_dirs_along_filepath(output_folder)
    result_deck_name = str()
    for deck in all_decks_in_config:
        deck_obj = load_deck(context, deck)
        context.file_utils.merge_all_cards_to_print(context, deck_obj, objective_cards_list, gambit_cards_list)
        result_deck_name += deck_obj.name[0:3] + '_'
    if len(result_deck_name) > 20:
        result_deck_name = str(uuid.uuid4())
    obj_name, gambit_name = result_deck_name + '_object_', result_deck_name + '_gambit_'
    context.file_utils.image_combiner(obj_name, objective_cards_list, (532, 744), 2)
    context.file_utils.image_combiner(gambit_name, gambit_cards_list, (532, 744), 2)


@step
@When('I merge to print all cards from decks folder')
def step_impl(context):
    objective_cards_list, gambit_cards_list = list(), list()
    decks = os.listdir(context.file_utils.decks_folder)
    output_folder = context.file_utils.for_printing_folder
    context.file_utils.create_all_dirs_along_filepath(output_folder)
    result_deck_name = str()
    for deck_file in decks:
        file_name = deck_file.replace(".json", "")
        deck_obj = load_deck(context, file_name)
        context.file_utils.merge_all_cards_to_print(context, deck_obj, objective_cards_list, gambit_cards_list)
        result_deck_name += deck_obj.name[0:3] + '_'
    if len(result_deck_name) > 20:
        result_deck_name = str(uuid.uuid4())
    obj_name, gambit_name = result_deck_name + '_object_', result_deck_name + '_gambit_'
    context.file_utils.image_combiner(obj_name, objective_cards_list, (532, 744), 2)
    context.file_utils.image_combiner(gambit_name, gambit_cards_list, (532, 744), 2)


@step
@When('I create template for "{back_file_name}"')
def step_impl(context, back_file_name):
    output_folder = context.file_utils.output_folder
    context.file_utils.create_folder(output_folder)
    context.file_utils.card_backs(back_file_name)


@step
@When('I clean all folders')
def step_impl(context):
    context.file_utils.clean_up()


@step
@Given('I open the "{deck_list}" page')
def step_impl(context, deck_list):
    if deck_list == "decks list":
        context.browser.get(context.deck_list_page.deck_list_url)


@step
@When('I select all decks up to date "{some_date}" and save them')
def step_impl(context, some_date):
    some_date = datetime.strptime(some_date, "%d %b %Y")
    decks_links_list = context.deck_list_page.get_deck_links_by_date(some_date)
    counter = 0
    for link in decks_links_list:
        counter += 1
        context.browser.get(link)
        file_name = 'deck' + str(counter)
        save_deck_as_json(context, file_name)


@step
@When('I merge to print only difference between deck "{deck_name}" and other decks in decks_list.json')
def step_impl(context, deck_name):
    all_decks_in_config = get_all_decks_urls_from_deck_list_file()
    deck_filename = deck_name + '.json'
    if not os.path.exists(os.path.join(context.file_utils.decks_folder, deck_filename)):
        context.deck_content_page.open_deck_page(deck_name)
        save_deck_as_json(context, deck_name)
    new_deck = load_deck(context, deck_name)
    new_deck_cards_dict = dict()
    for card in new_deck.cards:
        new_deck_cards_dict[card.name] = card
    set_of_old_cards = set()
    for name in all_decks_in_config:
        if name == deck_name:
            continue
        else:
            deck = load_deck(context, name)
            for card in deck.cards:
                set_of_old_cards.add(card.name)
    for card_name in new_deck_cards_dict:
        if card_name in set_of_old_cards:
            new_deck.remove_card(card_name)
    objective_cards_list, gambit_cards_list = list(), list()
    output_folder = context.file_utils.for_printing_folder
    context.file_utils.create_all_dirs_along_filepath(output_folder)
    context.file_utils.merge_all_cards_to_print(context, new_deck, objective_cards_list, gambit_cards_list)
    obj_name, gambit_name = deck_name + '_object_', deck_name + '_gambit_'
    context.file_utils.image_combiner(obj_name, objective_cards_list, (532, 744), 2)
    context.file_utils.image_combiner(gambit_name, gambit_cards_list, (532, 744), 2)


























