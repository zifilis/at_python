from selenium import webdriver
from configuration.config import config
from allure_behave.hooks import allure_report
import os
from features.utils.file_utils import FileUtils
from features.pages.deck_content_page import HomePage
from features.pages.deck_list_page import DeckListPage


def before_feature(context, feature):
    if str(config['browser'].lower()) == "firefox":
        context.browser = webdriver.Firefox(executable_path=config['path_to_local_webdriver'])
    elif str(config['browser'].lower()) == "chrome":
        context.browser = webdriver.Chrome(executable_path=config['path_to_local_webdriver'])
    else:
        context.browser = webdriver.Chrome()
    context.location = "https://www.underworldsdb.com"
    context.deck_content_page = HomePage(context.browser, context.location)
    context.deck_list_page = DeckListPage(context.browser, context.location)
    context.file_utils = FileUtils()
    context.list_of_decks = dict()


def after_feature(context, feature):
    context.browser.quit()


path_to_allure = os.path.join(os.path.dirname(__file__), '../allure/results')
allure_report(path_to_allure)
