from os import path
from features.card import Deck
import json
from features.utils.json_serializer import JsonEncoder
from features.utils.db_utils import *

output_folder = path.join(path.dirname(__file__), './../../output')
decks_folder = path.join(output_folder, 'decks')
resources_folder = path.join(path.dirname(__file__), './../../resources')


def get_deck_url_from_deck_list_file(deck_name):
    with open(path.join(resources_folder, 'decks_list.json')) as opened_file:
        chosen_decks = json.load(opened_file)
        return chosen_decks[deck_name]


def get_all_decks_urls_from_deck_list_file():
    with open(path.join(resources_folder, 'decks_list.json')) as opened_file:
        chosen_decks = json.load(opened_file)
        return chosen_decks


def get_deck_by_name(context, deck_name):
    cards_dict = context.deck_content_page.gather_all_cards_of_a_deck_by_card_type()
    deck_obj = Deck(deck_name, cards_dict)
    context.list_of_decks[deck_obj.name] = deck_obj
    return deck_obj


def load_deck(context, deck_name: str) -> Deck:
    if deck_name in context.list_of_decks:
        deck_obj = context.list_of_decks[deck_name]
    else:
        deck_name += '.json'
        file = path.join(decks_folder, deck_name)
        with open(file, 'r') as opened_file:
            deck_obj = Deck.decode_from_json(deck_name, json.load(opened_file))
    return deck_obj


def load_deck_from_db(context, deck_name: str) -> Deck:
    if deck_name in context.list_of_decks:
        deck_obj = context.list_of_decks[deck_name]
    else:
        deck_dict = get_deck_from_db_by_name(deck_name)
        deck_obj = Deck.decode_from_dict(deck_name, deck_dict)
    return deck_obj


def save_deck_as_json(context, deck_name, output_deck_folder=output_folder):
    deck_obj = get_deck_by_name(context, deck_name)
    file_name = deck_name + '.json'
    file_path = path.join(path.dirname(__file__), output_deck_folder, file_name)
    with open(file_path, 'w') as json_file:
        json.dump(deck_obj, json_file, cls=JsonEncoder, indent=4)


def get_dict_from_deck_obj(context, deck_name):
    deck_obj = get_deck_by_name(context, deck_name)
    return deck_obj.to_dict()

