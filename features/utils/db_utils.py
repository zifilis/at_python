from pymongo import MongoClient
from configuration.config import config

client = MongoClient(config['mongo_location'], config['mongo_port'])
db = client.whu
decks = db.decks


def save_dict_to_db(some_dict):
    if decks.find_one({"name": some_dict['name']}):
        print("Deck with the same name '%s' is already in the DB, overwriting..." % some_dict['name'])
        decks.delete_one({"name": some_dict['name']})
    else:
        decks.insert_one(some_dict)


def get_deck_from_db_by_name(deck_name):
    return decks.find_one({"name": deck_name})