from PIL import Image
import requests
from features.card import Card
import os
import errno
from features.utils.deck_utils import load_deck


class FileUtils:
    output_folder = os.path.join(os.path.dirname(__file__), './../../output')
    decks_folder = os.path.join(output_folder, 'decks')
    cards_folder = os.path.join(output_folder, 'cards')
    resources_folder = os.path.join(os.path.dirname(__file__), './../../resources')
    for_printing_folder = os.path.join(output_folder, 'for_printing')

    def create_folder(self, directory_path):
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

    def create_folders(self, directory_path, folders_list):
        for folder in folders_list:
            if not os.path.exists(os.path.join(directory_path, folder)):
                os.mkdir(os.path.join(directory_path, folder))

    def save_card_image(self, card: Card):
        card_full_path = os.path.join(self.output_folder, card.image_url)
        card_download_url = "https://www.underworldsdb.com/" + card.image_url
        if not os.path.exists(card_full_path):
            print("downloading card %s..." % card.name)
            self.create_all_dirs_along_filepath(card_full_path)
            image = requests.get(card_download_url)
            with open(card_full_path, 'wb') as file:
                file.write(image.content)

    def save_all_cards_images_of_all_decks(self, context):
        list_of_decks = os.listdir(self.decks_folder)
        set_of_cards = set()
        for name in list_of_decks:
            short_name = name.replace(".json", "")
            deck_obj = load_deck(context, short_name)
            for card in deck_obj.cards:
                if card.name in set_of_cards:
                    continue
                else:
                    self.save_card_image(card)
                    set_of_cards.add(card.name)

    def create_all_dirs_along_filepath(self, filepath):
        if not os.path.exists(os.path.dirname(filepath)):
            try:
                os.makedirs(os.path.dirname(filepath))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

    def chunks(self, file_list, length):
        for i in range(0, len(file_list), length):
            yield file_list[i:i + length]

    def card_backs(self, back_file_name):
        back = os.path.join(self.resources_folder, back_file_name)
        images = []
        counter = 0
        while counter < 9:
            images.append(back)
            counter += 1
        margin = 2
        card_size = (532, 744)
        folder_name = back_file_name.split('.')
        folder_name = folder_name[0]
        if "objective" in folder_name:
            color = (158, 129, 97)
        elif "power" in folder_name:
            color = (65, 73, 90)
        self.image_combiner(folder_name, images, card_size, margin, color)

    def image_combiner(self, name: str, images: list, card_size: tuple, margin: int, color=(255, 255, 255)):
        y_offset = 0
        canvas_width = card_size[0] * 3 + margin * 2
        canvas_height = card_size[1] * 3 + margin * 2
        chunk_counter = 0
        images = list(self.chunks(images, 9))
        for big_chunk in images:
            new_image = Image.new('RGBA', (canvas_width, canvas_height), color)
            cards_row = list(self.chunks(big_chunk, 3))
            for i in range(len(cards_row)):
                for j in range(len(cards_row[i])):
                    picture = Image.open(cards_row[i][j], mode="r")
                    if not picture.size == card_size:
                        picture = picture.resize(card_size, Image.ANTIALIAS)
                    x_offset = j * (card_size[0] + margin)
                    new_image.paste(picture, (x_offset, y_offset))
                y_offset = (i + 1) * (card_size[1] + margin)
            y_offset = 0
            chunk_counter += 1
            new_image_name = name + str(chunk_counter) + '.png'
            output_file = os.path.join(self.for_printing_folder, new_image_name)
            new_image.save(output_file)

    def merge_all_cards_to_print(self, context, deck_object, objective_cards_list, gambit_cards_list):
        for card in deck_object.cards:
            if card.card_type == 'objective':
                objective_cards_list.append(os.path.join(context.file_utils.output_folder, card.image_url))
            else:
                gambit_cards_list.append(os.path.join(context.file_utils.output_folder, card.image_url))


