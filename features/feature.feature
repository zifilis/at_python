Feature: smoke tests


  @smoke-tests
    Scenario: Download all decks, all cards and form papers to print
    Given I download all decks from decks_list.json
    Then I download all cards of all decks
    When I merge to print all cards from all decks in config file

  Scenario: Download all decks by the date, all cards and form papers to print
    Given I open the "decks list" page
    When I select all decks up to date "24 Aug 2019" and save them
    Then I download all cards of all decks
    When I merge to print all cards from decks folder

  Scenario: Download all decks, all cards and compare them to print only new cards
    Given I download all decks from decks_list.json
    Then I download all cards of all decks
    When I merge to print only difference between deck "zarbag2" and other decks in decks_list.json

  Scenario: Download zarbag's deck and save it to the DB
    Given I save deck into DB from decks_list.json by name "zarbag"

  Scenario: Download all decks, print main deck with additional cards from other decks
    Given I download deck "thorns1" and additional cards from other decks in list and merge these to print









