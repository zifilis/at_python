from features.pages.base_page import BasePage
from datetime import datetime


class DeckListPage(BasePage):

    deck_list_url = "https://www.underworldsdb.com/decks.php"

    page_elements = {
        "decks_dates": "sorting_1"
    }

    def get_deck_links_by_date(self, some_date):
        elements = self.browser.find_elements_by_class_name(self.page_elements["decks_dates"])
        links_list = list()
        for element in elements:
            if datetime.strptime(element.text, "%d %b %Y") <= some_date:
                break
            row = element.find_element_by_xpath("./..")
            row_cells = row.find_elements_by_xpath("./*")
            for cell in row_cells:
                if cell.get_attribute("data-filter") == "opvalid" or cell.get_attribute("data-filter") == "notvalid":
                    link = cell.find_element_by_xpath("./a").get_attribute("href")
                    links_list.append(link)
        return links_list