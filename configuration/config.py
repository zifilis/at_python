import os
import json

config = None


def load_config():
    global config
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.json')) as opened_file:
        config = json.load(opened_file)


load_config()