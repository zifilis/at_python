This project is dedicated to Warhammer Underworlds cards.
You need to put links to desired decks into /resources/decks_list.json
and then run program using "behave" command
Please note this project is created as tests - so you'll also need to edit
feature.feature file in features folder - list your deck name there just as you named it in json.
Cards will be downloaded and stored in /cards/ folder, as well as pages for printing will be stored
in  /output/ folder.

Enjoy!

PS Steps examples:
  Scenario: Download cursebreakers deck
    Given I open deck by URL "cursebreakers"
    Given I open the "decks list" page
    When I select all decks up to date "24 Aug 2019" and save them
    Then I download all cards of all decks
    Given I open deck by URL "cursebreakers"
    And I save the opened deck
    And I save all the decks listed in decks_list file
    And I download all cards from all decks in deck_list file
    And I download all cards from "cursebreakers" deck
    And I merge all cards from all decks to for_printing
    And I merge all cards from "cursebreakers" deck to for_printing
    
Typical scenarios:
  Scenario: Download all decks, all cards and form papers to print
    Given I download all decks from decks_list.json
    Then I download all cards of all decks
    When I merge to print all cards from all decks in config file
    
  Scenario: Download all decks by the date, all cards and form papers to print
    Given I open the "decks list" page
    When I select all decks up to date "24 Aug 2019" and save them
    Then I download all cards of all decks
    When I merge to print all cards from decks folder
    
  Scenario: Download all decks, all cards and compare them to print only new cards
    Given I download all decks from decks_list.json
    Then I download all cards of all decks
    When I merge to print only difference between deck "zarbag2" and other decks in decks_list.json

Scenario: Download zarbag's deck and save it to the DB
    Given I save deck into DB from decks_list.json by name "zarbag"
    
